﻿using Avia.BLL.Service;
using NUnit.Framework; 
using System.Collections.Generic;
using System.Linq; 
using Avia.Domain.Repositories;
using Avia.Domain.People;
using Moq;

namespace Avia.BLL.Test
{
    [TestFixture]
    public class AccountTest
    {

        IUofWork _unitOfWork;
        AccountManager _manager;


        [SetUp]
        public void AccountSetup()
        {
          
        }
 


        [Test]
        public void AuthorizeTest()
        {
            var db = new Mock<IUofWork>();

            db.Setup(d => d.Employes.GetAll())
                .Returns(new List<Employee>
                {
                     new Employee
                    {
                        FirstName= "test",
                        Login = "login",
                        Name= "def",
                         Password= "password",
                         Role=Domain.Enum.Role.Cassier
                    }
                }.AsQueryable());

            _unitOfWork = db.Object;


            _manager = new AccountManager(_unitOfWork);
            Employee empl = _manager.Athorize("login", "password");

            Assert.Equals(empl.Login, "login");
        }
    }
}
