﻿using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Avia.BLL;
using Avia.BLL.Interfaces;
using Avia.Domain.People;
using Avia.MVC.Controllers;

namespace Avia.MVC.Services
{
    public class UserService : IServiceUser
    {

        private readonly IAccountManager _accountManager;

        public UserService(IAccountManager manager)
        {
            _accountManager = manager;
        }

        public Employee GetCurrentUser()
        {
            var user = HttpContext.Current.Session["user"] as Employee;
            if (user == null)
            {
                var login = HttpContext.Current.User.Identity.Name;
                user = _accountManager.GetByLogin(login);
                HttpContext.Current.Session["user"] = user;
               
            }
            return user;
        }

        public ExceptionMessage Authorize(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(password))
                return new ExceptionMessage("Не правильный логин или пароль", true);

            var user = _accountManager.Athorize(login, password);

            if (user == null)
                return new ExceptionMessage("Не правильный логин или пароль", true);

            HttpContext.Current.Session["user"] = user;

            FormsAuthentication.SetAuthCookie(user.Login, false);
            return new ExceptionMessage("Пользователь " + user.FullName + " авторизован");
        }

        public void SignOut()
        {
            HttpContext.Current.Session["user"] = null;
            FormsAuthentication.SignOut();
        }


        public static IServiceUser Instance => DependencyResolver.Current.GetService(typeof(IServiceUser)) as UserService;
        public void Dispose()
        {
            _accountManager.Dispose();
        }
    }
}