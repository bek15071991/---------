﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; 

namespace Avia.MVC.Models.Account
{
    public class LoginVM
    {
        [Display(Name = "Логин")] 
        [Required]
        public string Login { get; set; }
        
        [Required]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
         
        public string Messages { get; set; }
    }
}