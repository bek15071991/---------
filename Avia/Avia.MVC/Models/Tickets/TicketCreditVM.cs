﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Avia.MVC.Models.Tickets
{
    public class TicketCreditVM
    {
       
        public int IdTicket { get; set; }

        public int IdClient { get; set; }

        public Dictionary<string,string> Tickets { get; set; }

        public  Dictionary<string,string> Clients { get; set; }


        [Display(Name = "Комментарий")]
        
        public string Comment { get; set; }

        [Display(Name = "Сумма оплаты клиента")]
        [Range(0,double.MaxValue)]
        public decimal Amount { get; set; }
        

    }
}