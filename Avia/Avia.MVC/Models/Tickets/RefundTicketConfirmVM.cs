﻿using System.ComponentModel.DataAnnotations;

namespace Avia.MVC.Models.Tickets
{
    public class RefundTicketConfirmVM
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Процент удержания")]
        public int? Persent { get; set; }
    }
}