﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;

namespace Avia.MVC.Models.Tickets
{
    public class TicketVm
    {

        public TicketVm()
        {
            Clients = new Dictionary<string, string>();
        }
        public int Id { get; set; }

        [Display(Name = "Номер")]
        [Required(ErrorMessage = "*")]
        public string Number { get; set; }

        [Display(Name = "Рейс")]
        [Required(ErrorMessage = "*")]
        public string Reis { get; set; }

        [Display(Name = "Тариф в валюте")]
        [Required(ErrorMessage = "*")]
        public decimal? TarifValut { get; set; }

        [Display(Name = "Курс")]
        [Required(ErrorMessage = "*")]
        public decimal? Kurs { get; set; }

        [Display(Name = "Тариф в сомах")]
        [Required(ErrorMessage = "*")]
        public decimal? TarifSom { get; set; }

        [Display(Name = "Аэропорт сбор")]
        [Required(ErrorMessage = "*")]
        public decimal? Aeroport { get; set; }

        [Display(Name = "Итого по билету")]
        [Required(ErrorMessage = "*")]
        public decimal? ItogoPoBiletu { get; set; }

        [Display(Name = "Комиссия в валюте")]
        [Required(ErrorMessage = "*")]
        public decimal? Comiso { get; set; }

        [Display(Name = "Комиссия в сомах")]
        [Required(ErrorMessage = "*")]
        public decimal? ComisoSom { get; set; }

        [Display(Name = "Скидка")]
        [Required(ErrorMessage = "*")]
        public decimal? Skidka { get; set; }

        [Display(Name = "Оплата пассажира")]
        [Required(ErrorMessage = "*")]
        public decimal? PayPass { get; set; }

        [Display(Name = "Итого в А/К")]
        [Required(ErrorMessage = "*")]
        public decimal? ItogoAv { get; set; }

        [Display(Name = "Топливный сбор")]
        [Required(ErrorMessage = "*")]
        public decimal? Toplivo { get; set; }

        [Display(Name = "Ф.И.О. клиента")]
        [Required(ErrorMessage = "*")]
        public string Passanger { get; set; }


        [Range(1, int.MaxValue)]
        public int CompanyId { get; set; }


        [Display(Name = "Компания")]
        public string CompanyString { get; set; }

        [IgnoreMap]
        public Dictionary<string, string> Companies { get; set; }
        public bool IsCredit { get; set; }

        [IgnoreMap]
        public int ClientId { get; set; }
        public Dictionary<string, string> Clients { get; set; }


        [Range(0, double.MaxValue)]
        [Display(Name = "Сумма оплаты клиента")]
        public decimal Amount { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

    }
}