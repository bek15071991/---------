﻿
using System.ComponentModel.DataAnnotations;

namespace Avia.MVC.Models.Settings
{
    public class ClientVM
    {
        
        public int Id { get; set; }

        [Display(Name = "Имя")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Отчество")] 
        public string SecondName { get; set; }
    }
}