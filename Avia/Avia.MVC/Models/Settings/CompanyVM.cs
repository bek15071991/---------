﻿using System.ComponentModel.DataAnnotations;

namespace Avia.MVC.Models.Settings
{
    public class CompanyVM
    {
        public int Id { get; set; }

        [Display(Name = "Процент")]
        [Required(ErrorMessage = "*")]
        public double Percent { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "*")]
        public string Name { get; set; }
    }
}