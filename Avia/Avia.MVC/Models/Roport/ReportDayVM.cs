﻿using System.Collections.Generic;
using Avia.Domain.Entyties;

namespace Avia.MVC.Models.Roport
{
    public class ReportDayVM
    {
        public List<Ticket> PayTickets { get; set; }
        public List<RefundTicket> RefundTickets { get; set; }
        public List<Ticket> CreditTickets { get; set; }

        public decimal Cash { get; set; }

        public decimal SummarySalled { get; set; }
        public decimal SummaryCredited { get; set; }
        public decimal SummaryRefunded { get; set; }

    }
}