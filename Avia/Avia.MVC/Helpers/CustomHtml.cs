﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Avia.Domain.Utils;

namespace Avia.MVC.Helpers
{
    public static class CustomHtml
    {
        public static MvcHtmlString CTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string place)
        {
            var memberAccessExpression = (MemberExpression)expression.Body;
            var attributes = memberAccessExpression.Member.GetCustomAttributes(
              typeof(DisplayAttribute), true);

            string name = string.Empty;

            if (attributes.Any())
            {
                var at = attributes[0] as DisplayAttribute;
                if (at != null)
                    name = at.Name;
            }

            var div = new TagBuilder("div");
            div.AddCssClass("form-group");


            div.InnerHtml = helper.LabelFor(expression)
                + helper.TextBoxFor(expression, new { @class = "form-control", @placeholder = place ?? name }).ToString();


            var error = helper.ValidationMessageFor(expression).ToString().Contains("field-validation-error");

            if (error)
                div.AddCssClass("has-error");

            return MvcHtmlString.Create(div.ToString());
        }


        public static MvcHtmlString CTextBoxForInputGroup<TModel, TProperty>(this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression, string place, string icon, string texticon)
        {
            var memberAccessExpression = (MemberExpression)expression.Body;
            var attributes = memberAccessExpression.Member.GetCustomAttributes(
              typeof(DisplayAttribute), true);

            string name = string.Empty;

            if (attributes.Any())
            {
                var at = attributes[0] as DisplayAttribute;
                if (at != null)
                    name = at.Name;
            }


            var div = new TagBuilder("div");

            div.AddCssClass("form-group");
            var ico = new TagBuilder("span");
            ico.AddCssClass("input-group-addon");
            var i = new TagBuilder("i");
            i.AddCssClass("text text-black");
            if (!string.IsNullOrWhiteSpace(icon))
                i.AddCssClass(icon);
            i.SetInnerText(texticon);
            ico.InnerHtml = i.ToString();



            var div2 = new TagBuilder("div");
            div2.AddCssClass("input-group");
            div2.InnerHtml =
            helper.TextBoxFor(expression, new { @class = "form-control", @placeholder = name ?? place })
                                .ToString() + ico;
            div.InnerHtml = helper.LabelFor(expression).ToString() + div2;

            var error = helper.ValidationMessageFor(expression).ToString().Contains("field-validation-error");

            if (error)
                div.AddCssClass("has-error");

            return MvcHtmlString.Create(div.ToString());

        }


        public static MvcHtmlString DropDownListFor<T>(this HtmlHelper<T> helper, string id, string titleName, Dictionary<string, string> list, string selectedId)
        {

            var d = helper.ValidationMessage(id).ToString().Contains("not valid");


            var div = new TagBuilder("div");
            div.AddCssClass("form-group");

            if (d)
                div.AddCssClass("has-error");

            var labale = new TagBuilder("label");
            labale.InnerHtml = titleName;


            var select = new TagBuilder("select");

            select.AddCssClass("form-control select2 ");
            select.Attributes["style"] = "width: 100%;";
            select.MergeAttribute("Id", id);
            select.MergeAttribute("Name", id);
            var options = "";

            var option = new TagBuilder("option");



            option.InnerHtml = "не выбрано";
            options += option.ToString();

            if (list != null)
                foreach (var item in list)
                {
                    option = new TagBuilder("option");
                    if (item.Key.Equals(selectedId))
                        option.MergeAttribute("selected", "selected");
                    option.MergeAttribute("Value", item.Key);
                    option.InnerHtml = item.Value;
                    options += option.ToString();

                }

            select.InnerHtml = options;

            div.InnerHtml = labale.ToString() + select.ToString();

            return MvcHtmlString.Create(div.ToString());
        }

        public static MvcHtmlString ModalWindow<T>(this HtmlHelper<T> helper, string idModal, TypeModal typeModal, string messages, string url, string nameButton)
        {
            var divModal = new TagBuilder("div");
            divModal.AddCssClass("modal fade");
            divModal.AddCssClass("modal-" + typeModal.ToString().ToLower());
            divModal.MergeAttribute("id", idModal);

            var dial = new TagBuilder("div");
            dial.AddCssClass("modal-dialog");

            var content = new TagBuilder("div");
            content.AddCssClass("modal-content");
            content.MergeAttribute("style", "background-color: coral;");

            var header = new TagBuilder("div");
            header.AddCssClass("modal-header");

            var button = new TagBuilder("button");
            button.MergeAttribute("type", "button");
            button.AddCssClass("close");
            button.MergeAttribute("data-dismiss", "modal");
            button.MergeAttribute("aria-label", "Close");

            var span = new TagBuilder("span");
            span.MergeAttribute("aria-hidden", "true");
            span.InnerHtml = "&times;";

            button.InnerHtml = span.ToString();

            var h = new TagBuilder("h4");
            h.AddCssClass("modal-title");


            if (typeModal == TypeModal.Danger)
                h.InnerHtml = "Удаление";
            else if (typeModal == TypeModal.Info)
                h.InnerHtml = "Инфо";
            else if (typeModal == TypeModal.Warning)
                h.InnerHtml = "Внимание";

            header.InnerHtml = button.ToString() + h.ToString();


            var body = new TagBuilder("div");
            body.AddCssClass("modal-body");


            var p = new TagBuilder("p");
            p.InnerHtml = messages;

            body.InnerHtml = p.ToString();

            var footer = new TagBuilder("div");
            footer.AddCssClass("modal-footer");

            var buttonSave = new TagBuilder("button");
            buttonSave.AddCssClass("btn btn-outline");
            buttonSave.MergeAttribute("type", "submit");

            buttonSave.InnerHtml = nameButton;

             

            var buttonClose = new TagBuilder("div");
            buttonClose.AddCssClass("btn btn-outline pull-left");
            buttonClose.MergeAttribute("type", "button");
            buttonClose.MergeAttribute("data-dismiss", "modal");
            buttonClose.InnerHtml = "Отмена";





            var form = new TagBuilder("form");
            form.MergeAttribute("action", url);

            form.InnerHtml = buttonSave.ToString() + buttonClose.ToString();

            footer.InnerHtml = form.ToString();

            content.InnerHtml = header + body.ToString() + footer;

            dial.InnerHtml = content.ToString();
            divModal.InnerHtml = dial.ToString();

            return MvcHtmlString.Create(divModal.ToString());


            //        < div class="modal modal-danger" id="DeleteModal">
            //    <div class="modal-dialog ">
            //        <div class="modal-content">
            //            <div class="modal-header">
            //                <button type = "button" class="close" data-dismiss="modal" aria-label="Close">
            //                    <span aria-hidden="true">&times;</span>
            //                </button>
            //                <h4 class="modal-title">Удаление</h4>
            //            </div>
            //            <div class="modal-body">
            //                <p>One fine body&hellip;</p>
            //            </div>
            //            <div class="modal-footer">

            //                <form action = "@Url.Action("DeleteTicket", "Ticket", new { id = 10 })" method="GET">

            //                <button type = "submit" class="btn btn-outline">Save changes</button>
            //                </form>

            //            </div>
            //        </div>
            //        <!-- /.modal-content -->
            //    </div>
            //    <!-- /.modal-dialog -->
            //</div>
        }


    }


    public enum TypeModal
    {
        [Description("Инфо")]
        Info,
        [Description("Удалить")]
        Danger,
        [Description("OK")]
        Success,
        [Description("Внимание")]
        Warning
    }
}