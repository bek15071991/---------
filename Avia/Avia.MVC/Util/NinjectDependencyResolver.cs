﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Avia.DAL.Repositories;
using Avia.Domain.Repositories;
using Ninject;

namespace Avia.MVC.Util
{
    public class NinjectDependencyResolver: IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
         
        private void AddBindings()
        {
            kernel.Bind<ITicketRepository>().To<TicketRepository>();
            kernel.Bind<IRefundTicketRepository>().To<RefundTicketRepository>();
            kernel.Bind(typeof(IBaseRepository<>)).To(typeof(BaseRepository<>));
            kernel.Bind<IEmployeRepository>().To<EmoloyeRepository>();
            kernel.Bind<IRepaymentRepository>().To<RepaymentRepository>();
            kernel.Bind<IClientRepository>().To<ClientRepository>();
         
        }
    }
}