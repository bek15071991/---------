﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Avia.BLL.Interfaces;
using Avia.Domain.Repositories;

namespace Avia.MVC.Controllers
{
     [Authorize]
    public class BaseController : Controller
    {
        protected IUnitofWork Db;
        protected IServiceUser ServiceUser; 

        public BaseController(IServiceUser service,  IUnitofWork db1)
        {
            this.Db = db1;
            ServiceUser = service; 
            ShowCountSalledTickets(); 
            ShowCountRefundedTickets();
        }

       
        private void ShowCountRefundedTickets()
        {
            ViewData["RefundedTickets"] = Db.RefundTickets.GetNotReport(ServiceUser.GetCurrentUser().Id).Count;
        }

          

         private void ShowCountSalledTickets()
         {
             ViewData["SoldTickets"] = Db.Tickets.GetListIsNotReport(ServiceUser.GetCurrentUser().Id).Count();
         }

         public string GetModelErrors()
         {
           return string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));
        }


      
    }
}