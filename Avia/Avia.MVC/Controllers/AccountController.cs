﻿using System;
using System.Web.Mvc;
using Avia.BLL.Interfaces;
using Avia.Domain.Repositories;
using Avia.MVC.Models.Account;

namespace Avia.MVC.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(IUnitofWork db1, IServiceUser service) : base(service, db1: db1)
        {
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginVM model)
        {
            if (ModelState.IsValid)
            {
               var result = ServiceUser.Authorize(model.Login, model.Password);
                if(result.IsExcept)
                    ModelState.AddModelError("ModelState", "Не правильный логин или пароль");
                else 
                return RedirectToAction("Index", "Home");
            } 
            model.Messages = GetModelErrors();
            return View(model);
        }


        public ActionResult SignOut()
        {
            ServiceUser.SignOut();
            return RedirectToAction("Login");
        }

       
    }
}