﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Avia.BLL.Interfaces;
using Avia.Domain.Entyties;
using Avia.Domain.People;
using Avia.Domain.Repositories;
using Avia.MVC.Models.Settings;

namespace Avia.MVC.Controllers
{

    public class SettingsController : BaseController
    {
        public SettingsController(IUnitofWork db1, IServiceUser service) : base(service, db1: db1)
        {
        }


        #region Авиакомпании

        [HttpGet]
        public ActionResult Companies()
        {
            var list = Db.Companies.GetAll().ToList();
            return View(list);
        }
         
        [HttpGet]
        public ActionResult AddCompany()
        {
            ViewBag.Title = "Добавление авиакомпании";
            ViewBag.Action = "add";
            ViewBag.Caption = "Новая авиакомпания";
            return View("FormCompany");
        }

        [HttpPost]
        public ActionResult AddCompany(CompanyVM model)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(expression => expression.CreateMap<CompanyVM, Company>());
                var company = Mapper.Map<CompanyVM, Company>(model);
                Db.Companies.Create(company);
                Db.Save();
                return RedirectToAction("Companies");
            }
            return View("FormCompany", model);
        }

        
        [HttpGet]
        public ActionResult EditCompany(int id)
        {
            var company = Db.Companies.GetNoTrack(id);
            if (company == null) return RedirectToAction("Companies");

            ViewBag.Title = "Изменение авиакомпании";
            ViewBag.Caption = "Авикомпания";
            ViewBag.Action = "edit";
            Mapper.Initialize(ex => ex.CreateMap<Company, CompanyVM>());
            var model = Mapper.Map<CompanyVM>(company);

            return View("FormCompany", model);
        }

        [HttpPost]
        public ActionResult EditCompany(CompanyVM model)
        {
            if (ModelState.IsValid)
            {
                var company = Db.Companies.GetNoTrack(model.Id);

                if (company != null)
                {
                    company.Percent = model.Percent;
                    company.Name = model.Name;
                    Db.Companies.Update(company);
                    Db.Save();
                    return RedirectToAction("Companies");
                }
                ModelState.AddModelError("IdCompany", "Не найдена компания");
            }
            ViewBag.Action = "edit";
            return View("FormCompany", model);
        }

        #endregion


        #region Клиенты

        [HttpGet]
        public ActionResult Clients()
        {
            var clients = Db.Clients.GetAll().ToList(); 
            return View(clients);
        }

        
        [HttpGet]
        public ActionResult AddClient()
        {
            ViewBag.Title = "Добавление клиета";
            ViewBag.Action = "add";
            ViewBag.Caption = "Новый клиент";
            return View("FormClient");
        }

        [HttpPost]
        public ActionResult AddClient(ClientVM model)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(expression => expression.CreateMap<ClientVM, Client>());
                var client = Mapper.Map<Client>(model);
                Db.Clients.Create(client);
                Db.Save();
                return RedirectToAction("Clients");
            }
            return View("FormClient", model);
        }

        [HttpGet]
        public ActionResult EditClient(int id)
        {
            var client = Db.Clients.GetNoTrack(id);
            if (client == null) return RedirectToAction("Clients");

            ViewBag.Title = "Изменение клиета";
            ViewBag.Action = "edit";
            ViewBag.Caption = "Клиент";
            Mapper.Initialize(ex => ex.CreateMap<Client, ClientVM>());
            var model = Mapper.Map<ClientVM>(client);

            return View("FormClient", model);
        }

        [HttpPost]
        public ActionResult EditClient(ClientVM model)
        {
         
            if (ModelState.IsValid)
            {
                var client = Db.Clients.GetNoTrack(model.Id);

               if(client==null)  ModelState.AddModelError("Id","Не найден клиент");

                Mapper.Initialize(expression => expression.CreateMap<ClientVM, Client>());

                var newClient = Mapper.Map(model, client);

                Db.Clients.Update(newClient);
                Db.Save();
                return RedirectToAction("Clients");
            }
            return View("FormClient");
        }

         
        #endregion


    }
}