﻿using System.Web.Mvc;
using Avia.BLL.Interfaces;
using Avia.MVC.Models.Roport;

namespace Avia.MVC.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private readonly IDaylyReport _daylyReport;

        public ReportController(IDaylyReport daylyReport)
        {
            _daylyReport = daylyReport; 
        }


        public ActionResult ReportDay()
        {
            var model = new ReportDayVM();
            _daylyReport.MakeReport();
            model.SummaryCredited = _daylyReport.SummaryCredited;
            model.SummaryRefunded = _daylyReport.SummaryRefunded;
            model.SummarySalled = _daylyReport.SummarySalled;
            model.CreditTickets = _daylyReport.GetSalledOnCreditTickets();
            model.PayTickets = _daylyReport.GetSoldTickets();
            model.RefundTickets = _daylyReport.GetRefundTickets();
            model.Cash = _daylyReport.GetCash();
            return View(model);
        }

        public ActionResult CloseReportToDay()
        {
            _daylyReport.Close();
            return RedirectToAction("Index", "Home");
        }
    }
}