﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using AutoMapper;
using Avia.BLL;
using Avia.BLL.Interfaces;
using Avia.BLL.Service;
using Avia.Domain.Entyties;
using Avia.Domain.People;
using Avia.Domain.Repositories;
using Avia.MVC.Models.Tickets;

namespace Avia.MVC.Controllers
{

    public class TicketController : BaseController
    {
        public TicketController(IUnitofWork db1, IServiceUser service) : base(service, db1: db1)
        {

            //Mapper.Initialize(cg => cg.CreateMap<RefundTicket, RefundTicketVM>().ForMember(d => d.NameWithAmount, c => c.MapFrom(d => d.Ticket.Passanger + " " + d.Ticket.PayPass)));


        }

        [HttpGet]
        public ActionResult AddTicket()
        {
            var model = new TicketVm();
            model.Companies = Db.Companies.GetAll().ToDictionary(company => company.Id.ToString(), company => company.Name);
            return View(model);
        }

        [HttpPost]
        public ActionResult AddTicket(TicketVm model)
        {
            if (ModelState.IsValid)
            {

                var seller = new Seller(Db, ServiceUser);
                Mapper.Initialize(cfg => cfg.CreateMap<TicketVm, Ticket>());
                var ticket = Mapper.Map<TicketVm, Ticket>(model);
                ticket.Company = Db.Companies.Get(model.CompanyId);
                var result = seller.SaleTicket(ticket);
                if (!result.IsExcept)
                    return RedirectToAction("ListTickets", "Ticket");
                ModelState.AddModelError("Error", result.Message);

            }
            model.Companies = Db.Companies.GetAll().ToDictionary(company => company.Id.ToString(), company => company.Name);
            return View(model);
        }

        [HttpGet]
        public ActionResult AddTicketCredit()
        {
            var model = new TicketCreditVM();
            model.Tickets = Db.Tickets.GetListIsNotReportIsNotCredit().ToDictionary(t => t.Id.ToString(), t => "Пассажир: " + t.Passanger + ", стоимость: " + t.PayPass + " сом.");
            model.Clients = Db.Clients.GetAll()
                .ToDictionary(d => d.Id.ToString(), d => d.FullName);
            return View(model);
        }
        [HttpPost]
        public ActionResult AddTicketCredit(TicketCreditVM model)
        {
            ExceptionMessage result = null;

            if (ModelState.IsValid)
            {
                var ticket = Db.Tickets.GetIsNotReport(model.IdTicket);

                var client = Db.Clients.Get(model.IdClient);

                var saller = new Seller(Db, ServiceUser);


                if (model.Amount > 0)
                    result = saller.SaleTicketOnCredit(ticket, client, model.Amount, model.Comment);
                else
                    result = saller.SaleTicketOnCredit(ticket, client);

                if (!result.IsExcept)
                {
                    TempData["Message"] = result.Message;
                    return RedirectToAction("ListTickets");
                }
                TempData["Error"] = result.Message;
                ModelState.AddModelError("Amount", "*");
            }
            model.Tickets = Db.Tickets.GetListIsNotReportIsNotCredit().ToDictionary(t => t.Id.ToString(), t => "Пассажир: " + t.Passanger + ", стоимость: " + t.PayPass + " сом.");
            model.Clients = Db.Clients.GetAll()
                .ToDictionary(d => d.Id.ToString(), d => d.FullName);
            return View(model);
        }

        [HttpGet]
        public ActionResult ListTickets()
        {
            var list = Db.Tickets.GetListIsNotReport().ToList();

            Mapper.Initialize(cfg => cfg.CreateMap<Ticket, TicketVm>().ForMember(d => d.CompanyString, c => c.MapFrom(d => d.Company.Name))
           .ForMember(d => d.IsCredit, c => c.MapFrom(cf => cf.CreditAmount > 0)));
            var tickets = Mapper.Map<List<TicketVm>>(list);

            return View(tickets);
        }

        [HttpGet]
        public ActionResult ListRefundTickets()
        {
            var list = Db.RefundTickets.GetListIsNotReport().ToList();

            Mapper.Initialize(cdg => cdg.CreateMap<RefundTicket, RefundTicketVM>()
            .ForMember(d => d.Aeroport, c => c.MapFrom(cd => cd.Ticket.Aeroport))
            .ForMember(d => d.Number, c => c.MapFrom(cd => cd.Ticket.Number))
            .ForMember(d => d.Reis, c => c.MapFrom(cd => cd.Ticket.Reis))
            .ForMember(d => d.TarifValut, c => c.MapFrom(cd => cd.Ticket.TarifValut))
            .ForMember(d => d.Kurs, c => c.MapFrom(cd => cd.Ticket.Kurs))
            .ForMember(d => d.TarifSom, c => c.MapFrom(cd => cd.Ticket.TarifSom))
            .ForMember(d => d.ItogoPoBiletu, c => c.MapFrom(cd => cd.Ticket.ItogoPoBiletu))
            .ForMember(d => d.Comiso, c => c.MapFrom(cd => cd.Ticket.Comiso))
            .ForMember(d => d.ComisoSom, c => c.MapFrom(cd => cd.Ticket.ComisoSom))
            .ForMember(d => d.Skidka, c => c.MapFrom(cd => cd.Ticket.Skidka))
            .ForMember(d => d.PayPass, c => c.MapFrom(cd => cd.Ticket.PayPass))
            .ForMember(d => d.ItogoAv, c => c.MapFrom(cd => cd.Ticket.ItogoAv))
            .ForMember(d => d.Toplivo, c => c.MapFrom(cd => cd.Ticket.Toplivo))
            .ForMember(d => d.Passanger, c => c.MapFrom(cd => cd.Ticket.Passanger))
            .ForMember(d => d.CompanyString, c => c.MapFrom(cd => cd.Ticket.Company.Name))
            .ForMember(d => d.IsCredit, c => c.MapFrom(cd => cd.Ticket.Credit != null))
            .ForMember(d => d.Amount, c => c.MapFrom(cd => cd.Amount))
            );

            var tickets = Mapper.Map<List<RefundTicketVM>>(list);

            return View(tickets);
        }

        [HttpGet]
        public ActionResult DeleteTicket(int id = 0)
        {
            var ticket = Db.Tickets.Get(id);
            if (ticket == null)
                return RedirectToAction("ListTickets");

            var credit = Db.Credits.GetByTicketId(ticket.Id);
            if (credit != null)
                Db.Credits.Delete(credit);
            Db.Tickets.Delete(ticket);
            Db.Save();
            TempData["Message"] = "Билет  на имя " + ticket.Passanger + " был удален.";
            TempData.Keep();

            return RedirectToAction("ListTickets");
        }

        [HttpGet]
        public ActionResult EditTicket(int id = 0)
        {
            var ticket = Db.Tickets.Get(id);

            if (ticket == null) return RedirectToAction("ListTickets");

            Mapper.Initialize(cfg => cfg.CreateMap<Ticket, TicketVm>().ForMember(d => d.CompanyId, c => c.MapFrom(d => d.Company.Id)));
            var model = Mapper.Map<Ticket, TicketVm>(ticket);
            model.Companies = Db.Companies.GetAll().ToDictionary(company => company.Id.ToString(), company => company.Name);
            if (ticket.Credit != null)
            {
                var client = ticket.Credit.Client;
                model.Clients.Add(client.Id.ToString(), client.FullName);
                model.ClientId = client.Id;
                model.Amount = ticket.PayPass - ticket.Credit.Amount;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult EditTicket(TicketVm model)
        {
            if (!ModelState.IsValid)
            {
                model.Companies = Db.Companies.GetAll().ToDictionary(comp => comp.Id.ToString(), comp => comp.Name);
                return View("EditTicket", model);
            }

            var ticket = Db.Tickets.Get(model.Id);

            var company = Db.Companies.Get(model.CompanyId);

            if (company == null)
            {
                ModelState.AddModelError("CompanyId", "*");
                return View("EditTicket", model);
            }

            if (ticket == null)
            {
                return RedirectToAction("ListTickets");
            }

            Mapper.Initialize(cfg => cfg.CreateMap<TicketVm, Ticket>());

            var newTiceket = Mapper.Map(model, ticket);
            newTiceket.Company = company;


            Db.Tickets.Update(newTiceket);
            Db.Save();
            TempData["Message"] = "Билет успешно обновлен";
            return RedirectToAction("ListTickets");

        }


        [HttpGet]
        public ActionResult RefundTicket()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RefundTicket(string value)
        {
            if (!Request.IsAjaxRequest())
                return HttpNotFound("Not found");

            var ticket = Db.Tickets.GetNotRefundByNumber(value);

            Mapper.Initialize(cg => cg.CreateMap<Ticket, RefundTicketVM>());

            var refTicket = Mapper.Map<RefundTicketVM>(ticket);

            if (string.IsNullOrWhiteSpace(value) || ticket == null)
                TempData["Error"] = "Билет не найден в базе";

            return PartialView("_RefundTicket", refTicket);
        }

        [HttpGet]
        public ActionResult RefundTicketConfirm(int id)
        {
            var model = new RefundTicketConfirmVM { Id = id };
            model.Persent = null;
            return View(model);
        }

        [HttpPost]
        public ActionResult RefundTicketConfirm(RefundTicketConfirmVM model)
        {
            var ticket = Db.Tickets.GetNotRefundById(model.Id);

            if (ticket == null)
                ModelState.AddModelError("Id", "*");

            if (!ModelState.IsValid)
                return View(model);

            var refunder = new RefunderTicket(Db, ServiceUser);
            var result = refunder.RefundTicket(model.Id, Convert.ToInt32(model.Persent));

            if (result.IsExcept)
            {
                TempData["Error"] = result.Message;
                return View(model);
            }
            TempData["Message"] = result.Message;
            return RedirectToAction("ListTickets");
        }


        [HttpGet]
        public ActionResult DeleteRefundTicket(int id = 0)
        {
            var refundTicket = Db.RefundTickets.Get(id);

            if (refundTicket == null)
                TempData["Error"] = "Билет не найден";

            Db.RefundTickets.Delete(refundTicket);
            Db.Save();

            return RedirectToAction("ListRefundTickets");

        }
    }
}