﻿using System.Threading;
using System.Web.Mvc;
using Avia.BLL.Interfaces;
using Avia.Domain.Repositories;

namespace Avia.MVC.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            Thread.Sleep(1500);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public HomeController(IUnitofWork db1, IServiceUser service) : base(service, db1: db1)
        {
        }
    }
}