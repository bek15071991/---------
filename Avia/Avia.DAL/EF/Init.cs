﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using Avia.Domain.Enum;
using Avia.Domain.People;

namespace Avia.DAL.EF
{
    internal class Init : DropCreateDatabaseIfModelChanges<AviaContext>
    {
        protected override void Seed(AviaContext context)
        {
            base.Seed(context);
             
            var cassier = new Employee
            {
                Login = "admin",
                Password = "def",
                Role = Role.Administrator,
                FirstName = "Ильгедеева",
                Name = "Айнура",   
                Image = File.ReadAllBytes(@"D:\repo\Avia\Avia.MVC\Content\Images\user2-160x160.jpg")
            };

            context.Employes.AddOrUpdate(cassier);
             
            context.SaveChanges();


        }
    }
}