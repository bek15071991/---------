﻿using Avia.Domain.Entyties;
using Avia.Domain.People;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Avia.DAL.EF.Config;

namespace Avia.DAL.EF
{
    public class AviaContext : DbContext
    {
        public AviaContext(string con)
            : base(con)
        {
            Database.SetInitializer(new Init());
        }

        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Repayment> Repayments { get; set; }
        public DbSet<Employee> Employes { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Credit> Credits { get; set; }

        public DbSet<RefundTicket> RefundTickets { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            #region Relations
            // Ticket
            modelBuilder.Entity<Ticket>().HasOptional(s => s.RefundTicket).WithRequired(d => d.Ticket);
            modelBuilder.Entity<Ticket>().HasRequired(d => d.Employee).WithMany(d => d.Tickets);
            modelBuilder.Entity<Ticket>().HasOptional(d => d.Credit).WithRequired(d => d.Ticket);
            modelBuilder.Entity<Ticket>().HasRequired(d => d.Company).WithMany(d => d.Tickets);
            
            //Credit
            modelBuilder.Entity<Credit>().HasRequired(d => d.Client).WithMany(d => d.Credits);
            modelBuilder.Entity<Credit>().HasMany(d => d.Repayments).WithRequired(d => d.Credit);
            modelBuilder.Entity<Credit>().HasRequired(d => d.Ticket).WithOptional(d => d.Credit);

            //RefundTicket 
            modelBuilder.Entity<RefundTicket>().HasRequired(d => d.Ticket).WithOptional(d => d.RefundTicket);

            //Client 
            modelBuilder.Entity<Client>().HasMany(d => d.Credits).WithRequired(d => d.Client);

            //Repayment
            modelBuilder.Entity<Repayment>().HasRequired(d => d.Credit).WithMany(d => d.Repayments);

           //Employee 
            modelBuilder.Entity<Employee>().HasMany(d => d.Tickets).WithRequired(d => d.Employee);

            //Company
            modelBuilder.Entity<Company>().HasMany(d => d.Tickets).WithRequired(d => d.Company);

            #endregion


            modelBuilder.Properties().Where(pr => pr.Name == "Id").Configure(p => p.IsKey());
            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("nvarchar"));
            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(150));


            modelBuilder.Entity<Ticket>().ToTable("Tickets");
            modelBuilder.Entity<Client>().ToTable("Clients");
            modelBuilder.Entity<RefundTicket>().ToTable("RefundTickets");
            modelBuilder.Entity<Company>().ToTable("Companies");
            modelBuilder.Entity<Credit>().ToTable("Credits");
            modelBuilder.Entity<Repayment>().ToTable("Repayments");
            modelBuilder.Entity<Employee>().ToTable("Employees");





            modelBuilder.Configurations.Add(new TicketConfig());
        }

    }
}
