﻿using Avia.DAL.EF;
using Avia.Domain.People;
using Avia.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avia.DAL.Repositories
{
  public  class ClientRepository : BaseRepository<Client>, IClientRepository
    {
      public ClientRepository(AviaContext db):base(db)
      {

      }

      public bool Exists(int id)
      {
          return GetNoTrack(id) != null;
      }

      public Client GetNoTrack(int id)
      {
          return Db.Clients.AsNoTracking().FirstOrDefault(d => d.Id.Equals(id));
      }
    }
}
