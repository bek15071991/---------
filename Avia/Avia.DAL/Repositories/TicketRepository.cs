﻿using System.Collections.Generic;
using System.Data.Entity;
using Avia.DAL.EF;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;
using System.Linq;

namespace Avia.DAL.Repositories
{
    public class TicketRepository : BaseRepository<Ticket>, ITicketRepository
    {
        public TicketRepository(AviaContext db)
            : base(db)
        {

        }

        public IQueryable<Ticket> GetListIsNotReport(int id)
        {
            return GetListIsNotReport().Where(d => d.Employee.Id == id);
        }


        public IQueryable<Ticket> GetListIsNotReport()
        {
            return GetAll().Where(d => d.IsReport == false);
        }

        public Ticket GetIsNotReport(int idTicket)
        {
            return GetListIsNotReport().AsNoTracking().FirstOrDefault(d => d.Id.Equals(idTicket));
        }



        public bool Exists(int id)
        {
            return Get(id) != null;
        }

        public IQueryable<Ticket> GetListIsNotReportIsNotCredit()
        {
            return Db.Tickets.Where(d => d.Credit == null && d.IsReport == false);
        }

        public Ticket GetNotRefundByNumber(string value)
        {
            var ticket = Db.Tickets.FirstOrDefault(d => d.Number.Equals(value));
            if (ticket == null || Db.RefundTickets.Any(d => d.Ticket.Id == ticket.Id))
                return null;
            return ticket;
        }

        public Ticket GetNotRefundById(int id)
        {
            if (Db.RefundTickets.Any(d => d.Ticket.Id.Equals(id)))
                return null;
            return Db.Tickets.FirstOrDefault(d => d.Id.Equals(id));
        }

        public List<Ticket> GetListNotReportCredit()
        {
            return Db.Tickets.Where(d => !d.IsReport && d.Credit != null).ToList();
        }

        public List<Ticket> GetNotReportSalled(int userId)
        {
            return Db.Tickets.Where(d => !d.IsReport && d.Credit == null).ToList();
        }

        public List<Ticket> GetNotReportCredited(int userId)
        {
            return Db.Tickets.Where(d => !d.IsReport && d.Credit != null).ToList();
        }
    }
}
