﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Avia.DAL.EF;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace Avia.DAL.Repositories
{
   public class RefundTicketRepository :BaseRepository<RefundTicket>, IRefundTicketRepository
    {
       public RefundTicketRepository(AviaContext db) : base(db)
       { 
       }

       public IQueryable<RefundTicket> GetListIsNotReport()
       {
           return GetAll().Where(d => d.IsReport == false);
       }

        public  RefundTicket   GetIsNotReport(int id)
        {
            return GetListIsNotReport().FirstOrDefault(d => d.Id.Equals(id));
        }

       public RefundTicket GetNoTrack(int id)
       {
           return GetAll().FirstOrDefault(d => d.Id.Equals(id));
       }
         

       public List<RefundTicket> GetNotReport(int userId)
       {
           return Db.RefundTickets.Where(d => !d.IsReport && d.Ticket.Employee.Id == userId).ToList();
       }
    }
}
