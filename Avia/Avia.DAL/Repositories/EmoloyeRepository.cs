﻿using Avia.DAL.EF;
using Avia.Domain.People;
using Avia.Domain.Repositories;
using System.Linq;

namespace Avia.DAL.Repositories
{
    public class EmoloyeRepository : BaseRepository<Employee>, IEmployeRepository
    {
        public EmoloyeRepository(AviaContext db)
            : base(db)
        {

        }

        

        public Employee GetByLoginPassword(string login, string password)
        {
            return Db.Employes.FirstOrDefault(user => user.Login.Equals(login.Trim())
            & user.Password.Equals(password.Trim()));
        }

        public Employee GetByLogin(string login)
        {
            return Db.Employes.FirstOrDefault(user => user.Login.Equals(login));
        }

        public Employee GetNoTrack(int id)
        {
            return Db.Employes.FirstOrDefault(d => d.Id.Equals(id));
        }
    }
}
