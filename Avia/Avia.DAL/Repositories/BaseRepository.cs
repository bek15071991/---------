﻿using Avia.DAL.EF;
using Avia.Domain.Repositories;
using System;
using System.Data.Entity;
using System.Linq;

namespace Avia.DAL.Repositories
{
    public abstract class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : class
    {
        protected AviaContext Db;

        protected BaseRepository(AviaContext db)
        {
            Db = db;
        }
        public IQueryable<T> GetAll()
        {
            return Db.Set<T>();
        }

        public void Create(T t)
        {
            Db.Set<T>().Add(t);
        }

        public void Update(T t)
        {
            Db.Entry(t).State =EntityState.Modified;
        }

        public void Delete(T t)
        {
            Db.Set<T>().Remove(t);
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.Collect();
        }


        public void SaveChanges()
        {
            Db.SaveChanges();
        }

        public T Get(int id)
        {
            return Db.Set<T>().Find(id);
        }
    }
}
