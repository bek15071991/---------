﻿using Avia.DAL.EF;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avia.DAL.Repositories
{
    public class RepaymentRepository : BaseRepository<Repayment>, IRepaymentRepository
    {
        public RepaymentRepository(AviaContext db)
            : base(db)
        {

        }
    }
}
