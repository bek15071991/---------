﻿using System.Linq;
using Avia.DAL.EF;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace Avia.DAL.Repositories
{
  public  class CreitRepository :BaseRepository<Credit>, ICreditRepository
    {
      public CreitRepository(AviaContext db) : base(db)
      {
      }

      public Credit GetByTicketId(int id)
      {
          return Db.Credits.FirstOrDefault(d => d.Ticket.Id.Equals(id));
      }
    }
}
