﻿using System.Linq;
using Avia.DAL.EF;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace Avia.DAL.Repositories
{
    public class CompanyRepository : BaseRepository<Company>, ICompanyRepositories
    {
        public CompanyRepository(AviaContext db) : base(db)
        {
        }

        public Company GetNoTrack(int companyId)
        {
            return Db.Companies.AsNoTracking().FirstOrDefault(d => d.Id.Equals(companyId));
        }
    }
}
