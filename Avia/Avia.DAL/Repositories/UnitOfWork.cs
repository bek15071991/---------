﻿using Avia.DAL.EF;
using Avia.Domain.Repositories;
using System;

namespace Avia.DAL.Repositories
{
    public class UnitOfWork : IUnitofWork
    {

        private AviaContext _db;
        private IClientRepository _clients;
        private IRepaymentRepository _repayments;
        private IEmployeRepository _employes;
        private ITicketRepository _tickets;
        private ICreditRepository _creditRepository;
        private IRefundTicketRepository _refundTicketRepository;
        private ICompanyRepositories _companyRepositories;

        public UnitOfWork(string conString)
        {
            _db = new AviaContext(conString);
            _clients =  new ClientRepository(_db);
            _repayments = new RepaymentRepository(_db);
            _employes =  new EmoloyeRepository(_db);
            _tickets = new TicketRepository(_db);
            _creditRepository = new  CreitRepository(_db);
            _refundTicketRepository = new RefundTicketRepository(_db);
            _companyRepositories = new CompanyRepository(_db);

        }

        public IClientRepository Clients
        {
            get { return _clients; }
        }

        public ITicketRepository Tickets
        {
            get
            {
                return _tickets;
            } 
        }

        public IEmployeRepository Employes
        {
            get
            {
                return _employes;
            } 
        }

        public ICreditRepository Credits { get { return _creditRepository; } }

        public IRepaymentRepository Repayments
        {
            get
            {
                return _repayments;
            } 
        }

        public IRefundTicketRepository RefundTickets { get { return _refundTicketRepository; } }
        public ICompanyRepositories Companies { get { return _companyRepositories; } }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
