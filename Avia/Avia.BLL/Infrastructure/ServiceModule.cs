﻿using Avia.BLL.Interfaces;
using Avia.BLL.Service;
using Avia.DAL.Repositories;
using Avia.Domain.Repositories;
using Ninject.Modules;

namespace Avia.BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private readonly string connectionString;
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitofWork>().To<UnitOfWork>().WithConstructorArgument(connectionString);
            Bind<IAccountManager>().To<AccountManager>();

        }
    }
}
