﻿using System;
using System.Collections.Generic;  
using Avia.Domain.Entyties;

namespace Avia.BLL.Interfaces
{
    public interface IDaylyReport : IDisposable
    {
        List<Ticket> GetSoldTickets();

        List<RefundTicket> GetRefundTickets();

        List<Ticket> GetSalledOnCreditTickets();

        void MakeReport();
         
        decimal GetCash();
         
        decimal SummarySalled { get; }
        decimal SummaryCredited { get; }
        decimal SummaryRefunded { get; }

        void Close();
    }
}
