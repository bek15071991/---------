﻿using System;
using Avia.Domain.Enum;
using Avia.Domain.People;

namespace Avia.BLL.Interfaces
{
    public interface IAccountManager :IDisposable
    {
        Employee Athorize(string login, string password);

        Employee GetByLogin(string login);

        ExceptionMessage CreateAccount(string login, string password, string name, string firstName, Role role);

        ExceptionMessage ChangePasswordByLogin(string login, string newpassword); 
        ExceptionMessage ChangeLoginById(int id, string newlogin);

    }
}
