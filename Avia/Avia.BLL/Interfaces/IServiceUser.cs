﻿using System;
using Avia.Domain.People;

namespace Avia.BLL.Interfaces
{
   public interface IServiceUser : IDisposable
   {
       Employee GetCurrentUser();

       ExceptionMessage Authorize(string login, string password);

       void SignOut();
   }


    
}
