﻿using System;
using System.Transactions;
using Avia.BLL.Interfaces;
using Avia.Domain.Entyties;
using Avia.Domain.People;
using Avia.Domain.Repositories;

namespace Avia.BLL.Service
{
    public class Seller : IDisposable
    {
        private readonly IUnitofWork _db;
        private readonly IServiceUser _serviceUser;

        public Seller(IUnitofWork db, IServiceUser serviceUser)
        {
            _db = db;
            _serviceUser = serviceUser;
        }

        public ExceptionMessage SaleTicket(Ticket ticket)
        {
            if (ticket == null)
                return new ExceptionMessage("Не укзан билет", true);

            if (ticket.Company == null)
                return new ExceptionMessage("Не укзана авиакомпания", true);


            ticket.Employee = _db.Employes.Get(_serviceUser.GetCurrentUser().Id);
            ticket.Date = DateTime.Now;

            _db.Tickets.Create(ticket);
            _db.Save();
            return new ExceptionMessage("Билет успешно продан", false);
        }

        public ExceptionMessage SaleTicketOnCredit(Ticket ticket, Client client)
        {
            if (ticket == null)
                return new ExceptionMessage("Не указан билет", true);

            if (client == null)
                return new ExceptionMessage("Не указан клиент", true);

            if (ticket.Company == null)
                return new ExceptionMessage("Не укзана авиакомпания", true);

            if (!_db.Clients.Exists(client.Id))
                return new ExceptionMessage("Не найден указанный клиент", false);

             
                var credit = new Credit();
                credit.Ticket= ticket;
                credit.Client = client;
                credit.Amount = ticket.PayPass; 
                _db.Tickets.Update(ticket);  
                _db.Credits.Create(credit);

                _db.Save(); 
            return new ExceptionMessage("Билет умпешно продан в долг");
        }



        public ExceptionMessage SaleTicketOnCredit(Ticket ticket, Client client, decimal cash, string comment)
        {
            if (ticket == null)
                return new ExceptionMessage("Не указан билет", true);

            if (client == null)
                return new ExceptionMessage("Не указан клиент", true);

            if (!_db.Clients.Exists(client.Id))
                return new ExceptionMessage("Не найден указанный клиент", true);

            if (cash < 1)
                return new ExceptionMessage("Не указанна сумма платежа", true);

            if (ticket.PayPass <= cash)
                return new ExceptionMessage("Оплата превышает сумму за билет", true);

          
            var credit = new Credit();
           credit.Ticket=ticket;
            credit.Client = client;
            credit.Amount = ticket.PayPass -cash;
            credit.Comment = comment;
             
            _db.Tickets.Update(ticket);

            _db.Credits.Create(credit);

            _db.Save();
            return new ExceptionMessage("Билет умпешно продан в долг");
        }

        public void Dispose()
        {
            _db.Dispose();
            _serviceUser.Dispose();
        }
    }
}