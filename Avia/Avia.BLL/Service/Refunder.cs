﻿using System;
using Avia.BLL.Interfaces;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace Avia.BLL.Service
{
    public class RefunderTicket : IDisposable
    {
        private readonly IUnitofWork _db;
        private readonly IServiceUser _serviceUser;

        public RefunderTicket(IUnitofWork unitofWork, IServiceUser serviceUser)
        {
            _db = unitofWork;
            _serviceUser = serviceUser;
        }

        public ExceptionMessage RefundTicket(int idTicket)
        {
            var ticket = _db.Tickets.Get(idTicket);
            if (ticket == null)
                return new ExceptionMessage("Не найден билет", true);

            var refundTicket = new RefundTicket
            {
                Ticket =  ticket ,
                Amount = ticket.PayPass, 
                Date = DateTime.Now
            };
            _db.RefundTickets.Create(refundTicket);
            _db.Save();
            return new ExceptionMessage("Возврат успешно проведен");
        }

        public ExceptionMessage RefundTicket(int idTicket, int persent)
        {
            var ticket = _db.Tickets.Get(idTicket);
            if (ticket == null)
                return new ExceptionMessage("Не найден билет", true);

            if (persent <= 0)
                return new ExceptionMessage("Не указан процент", true);

            if (persent > 100)
                return new ExceptionMessage("Процент указан не правильно", true);

            var refundTicket = new RefundTicket
            {
                Ticket = ticket ,
                Amount = (ticket.PayPass / 100) * persent, 
                Date = DateTime.Now
            };
            _db.RefundTickets.Create(refundTicket);
            _db.Save();
            return new ExceptionMessage("Возврат успешно проведен");
        }

        public void Dispose()
        {
            _db.Dispose();
            _serviceUser.Dispose();
        }
    }
}