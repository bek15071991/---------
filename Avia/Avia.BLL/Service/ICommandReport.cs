﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using Avia.Domain.Entyties;

namespace Avia.BLL.Service
{
    public interface ICommandReport :IDisposable
    {
        void GeneratePreview();

          List<Ticket> Tickets { get; set; }

          List<RefundTicket> RefundTickets { get; set; }

          decimal Cash { get; set; }

        void CloseDayReport();
    }
}
