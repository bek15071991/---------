﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avia.BLL.DTO.Report;
using Avia.BLL.Interfaces;
using Avia.Domain.Repositories;

namespace Avia.BLL.Service
{
    public class ReportService  
    {
        private readonly IUnitofWork _db;

        public ReportService(IUnitofWork db)
        {
            _db = db;
        }


        public List<TicketDTO> GetPreview()
        {
            var list = new List<TicketDTO>();

            list = _db.Tickets.GetListIsNotReport().Select(t => new TicketDTO()
            {
                Id = t.Id,
                Passanger = t.Passanger,
                ItogAvia = t.ItogoAv,
                NumberTicket = t.Number,
                PassangerPay = t.PayPass,
                TypeTicket = 1
            }).ToList();


            list.AddRange(
                _db.RefundTickets.GetListIsNotReport().Select(t => new TicketDTO
                {
                    Id = t.Id,
                    Passanger = t.Ticket.Passanger,
                    ItogAvia = t.Ticket.ItogoAv,
                    NumberTicket = t.Ticket.Number,
                    PassangerPay = t.Ticket.PayPass,
                    TypeTicket = 2

                }).ToList()
                );
            return list;
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
