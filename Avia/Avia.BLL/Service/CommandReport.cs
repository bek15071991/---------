﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Avia.BLL.Interfaces;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace Avia.BLL.Service
{
    public class CommandReport : ICommandReport
    {
        private IServiceUser _service;
        private readonly IUnitofWork _unit;
        private bool IsGenerate;

        public CommandReport(IUnitofWork unit, IServiceUser service)
        {
            this._unit = unit;
            this._service = service;
        }

        public List<Ticket> Tickets { get; set; }

        public List<RefundTicket> RefundTickets { get; set; }

        public decimal Cash { get; set; }

        public void GeneratePreview()
        {
            Tickets = _unit.Tickets.GetListIsNotReport().ToList();
            RefundTickets = _unit.RefundTickets.GetListIsNotReport().ToList();
            Cash = Tickets.Sum(t => t.PayPass) - RefundTickets.Sum(d => d.Amount);
            IsGenerate = true;
        }
        public void CloseDayReport()
        {
            if (!IsGenerate) GeneratePreview();

            foreach (var ticket in Tickets)
            {
                ticket.IsReport = true;
                _unit.Tickets.Update(ticket);
            }

            foreach (var refundTicket in RefundTickets)
            {
                refundTicket.IsReport = true;
                _unit.RefundTickets.Update(refundTicket);
            }

            _unit.Save();
            RefundTickets = null;
            Tickets = null;
            Cash = 0;
        }

        public void Dispose()
        {
            _unit.Dispose();
            _service.Dispose();
        }
    }
}