﻿using System.Collections.Generic;
using System.Linq;
using Avia.BLL.Interfaces;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace Avia.BLL.Service
{
    public class DaylyReport : IDaylyReport
    {
        private readonly IUnitofWork _db;
        private readonly IServiceUser _serviceUser;

        private List<Ticket> _tickets;
        private List<Ticket> _creditTickets;
        private List<RefundTicket> _refundTickets;

        public DaylyReport(IUnitofWork db, IServiceUser serviceUser)
        {
            _db = db;
            _serviceUser = serviceUser;
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<Ticket> GetSoldTickets()
        {
            return _tickets;
        }

        public List<RefundTicket> GetRefundTickets()
        {

            return _refundTickets;
        }

        public List<Ticket> GetSalledOnCreditTickets()
        {

            return _creditTickets;
        }

        public void MakeReport()
        {
            var userId = _serviceUser.GetCurrentUser().Id;

            _tickets =
                _db.Tickets.GetNotReportSalled(userId).ToList();
            _refundTickets = _db.RefundTickets.GetNotReport(userId);

            _creditTickets = _db.Tickets.GetNotReportCredited(userId);

        }


        public decimal GetCash()
        {
            SummaryCredited = _creditTickets.Sum(c => c.PayPass - c.CreditAmount);
            SummarySalled = _tickets.Sum(c => c.PayPass);
            SummaryRefunded = _refundTickets.Sum(r => r.Amount);

            var result = SummarySalled + SummaryCredited - SummaryRefunded;

            return result;
        }

        public decimal SummarySalled { get; private set; }
        public decimal SummaryCredited { get; private set; }
        public decimal SummaryRefunded { get; private set; }
        public void Close()
        {
            var id = _serviceUser.GetCurrentUser().Id;

            var salles = _db.Tickets.GetListIsNotReport(id);

            foreach (var ticket in salles)
            {
                ticket.IsReport = true;
            }

            foreach (var refundTicket in _db.RefundTickets.GetNotReport(id))
            {
                refundTicket.IsReport = true;
            }

            _db.Save();
        }
    }
}
