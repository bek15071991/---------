﻿using System;
using System.Linq;
using Avia.BLL.Interfaces;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace Avia.BLL.Service
{
    public class Repaymenter
    {
        private readonly IUnitofWork _db;
        private IServiceUser _serviceUser;

        public Repaymenter(IUnitofWork unitofWork, IServiceUser serviceUser)
        {
            _db = unitofWork;

            _serviceUser = serviceUser;
        }


        public ExceptionMessage Repay(decimal cash, int idTicket, string comment)
        {
            if (cash < 1)
                return new ExceptionMessage("Не указана сумма погашения", true);

            var ticket = _db.Tickets.Get(idTicket);

            if (ticket == null)
                return new ExceptionMessage("Не указан билет", true);

            if (ticket.IsPay)
                return new ExceptionMessage("Билет уже оплачен", true);

            var credits = _db.Credits.GetByTicketId(idTicket);

            if(credits==null)
                return new ExceptionMessage("Билет не найден", true);

            var credit = credits.Amount - credits.Repayments.Sum(d => d.Amount);
            if (credit < cash)
                return new ExceptionMessage("Сумма погашение превышает долг", true);


            var repayment = new Repayment(); 
            repayment.DateRepayment = DateTime.Now;
            repayment.Amount = cash;
            repayment.Comment = comment;
            _db.Repayments.Create(repayment);
            _db.Save();
            return new ExceptionMessage("Погашение успешно проведено");
        }
    }
}