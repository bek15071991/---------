﻿using Avia.Domain.Repositories;
using System;
using System.Runtime.Remoting;
using Avia.BLL.Interfaces;
using Avia.Domain.Enum;
using Avia.Domain.People;

namespace Avia.BLL.Service
{
    public class AccountManager : IAccountManager
    {
        readonly IUnitofWork _db;

        public AccountManager(IUnitofWork db)
        {
            _db = db;
        }

        public Employee Athorize(string login, string password)
        {
            return _db.Employes.GetByLoginPassword(login, password);
        }

        public Employee GetByLogin(string login)
        {
            return string.IsNullOrEmpty(login) ? null
                : _db.Employes.GetByLogin(login.Trim());
        }

        public ExceptionMessage CreateAccount(string login, string password, string name, string firstName, Role role)
        {
            if (string.IsNullOrEmpty(login)) return new ExceptionMessage("Не указан логин", true);
            if (string.IsNullOrEmpty(password)) return new ExceptionMessage("Не указан пароль", true);
            if (string.IsNullOrEmpty(name)) return new ExceptionMessage("Не указана имя", true);
            if (string.IsNullOrEmpty(firstName)) return new ExceptionMessage("Не указана фамилия", true);
            if (role < 0 || Enum.GetValues(typeof(Role)).Length <= (int)role)
                return new ExceptionMessage("Не указан роль", true);
            if (GetByLogin(login) != null) return new ExceptionMessage("Такой аккаунт существует", true);


            var emploey = new Employee
            {
                Login = login,
                Password = password,
                Name = name,
                FirstName = firstName,
                Role = role
            };

            _db.Employes.Create(emploey);
            _db.Save();
            return new ExceptionMessage("Аккаунт был успешно сохранен");
        }

        public ExceptionMessage ChangePasswordByLogin(string login, string newpassword)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(newpassword))
                return new ExceptionMessage("Логин или пароль не были указаны", true);



            if (GetByLogin(login) == null)
                return new ExceptionMessage("Аккаунт не был найден", true);

            var val = _db.Employes.GetByLogin(login);
            val.Password = newpassword;
            _db.Employes.Update(val);
            _db.Save();
            return new ExceptionMessage("Пароль успешно изменен");
        }

        public ExceptionMessage ChangeLoginById(int id, string newlogin)
        {

            if (string.IsNullOrWhiteSpace(newlogin))
                return new ExceptionMessage("Не указан новый логин", true);

            var employe = _db.Employes.GetNoTrack(id);

            if (employe == null)
                return new ExceptionMessage("Аккаунт не найден", true);

            employe.Login = newlogin;

            _db.Employes.Update(employe);

            _db.Save();
            return new ExceptionMessage("Логин успешно изменен", false);
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
