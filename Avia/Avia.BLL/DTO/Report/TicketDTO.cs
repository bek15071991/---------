﻿using System.Data;

namespace Avia.BLL.DTO.Report
{
    public class TicketDTO
    {
        public int Id { get; set; }
        public string Passanger { get; set; }
        public string NumberTicket { get; set; } 
        public decimal PassangerPay { get; set; }
        public decimal ItogAvia { get; set; }
         

        public int TypeTicket { get; set; }
    }
}
