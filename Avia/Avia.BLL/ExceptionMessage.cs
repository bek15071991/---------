﻿using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace Avia.BLL
{
    public class ExceptionMessage
    {
        public ExceptionMessage(string message, bool isExcept)
        {
            Message = message;
            IsExcept = isExcept;
        }

        public ExceptionMessage(string message)
        {
            Message = message;
        }

        public string Message { get; set; }

        public bool IsExcept { get; set; }
    }
}
