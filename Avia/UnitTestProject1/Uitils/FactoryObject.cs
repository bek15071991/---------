﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avia.Domain.Entyties;
using Avia.Domain.Enum;
using Avia.Domain.People;
using Avia.Domain.Repositories;
using Moq;
using UnitTestProject1.Repositories;

namespace UnitTestProject1.Uitils
{
    public class FactoryObject
    {
        public static Mock<IUnitofWork> GetDb()
        {
            var mock = new Mock<IUnitofWork>();
            mock.Setup(d => d.Employes).Returns(() => new StubEmployeRepository());
            mock.Setup(d => d.Clients).Returns(() => new StubClientRepository());
            mock.Setup(d => d.Tickets).Returns(() => new StubTicketRepository());
            mock.Setup(d => d.Repayments).Returns(() => new StubRepaymentRepositories());

            return mock;
        }


        public static IUnitofWork GetUnit()
        {
            return new StubUnitOfWork();
        }

        public static Ticket GetTicket()
        {
            return new Ticket
            {
                Id = 1,
                Date = DateTime.Now,
                Company = new Company(),
                Number = "6242127668020",
                Passanger = "Namasalieva",
                Reis = "560",
                Kurs = 62,
                TarifValut = 150,
                TarifSom = 9300,
                Comiso = 20,
                ComisoSom = 1240,
                ItogoPoBiletu = 10540,
                ItogoAv = 9300,
                Skidka = 620,
                PayPass = 9920
            };
        }


        public static List<Ticket> GetTickets()
        {
            var list = new List<Ticket>();

            list.Add(GetNewTicket());
            list.Add( new Ticket
            {
                Id = 2,
                Date = DateTime.Now,
                Company = new Company(),
                Number = "1382401416296",
                Passanger = "МАМЫРБЕКОВА",
                Reis = "аю11",
                Kurs = 1,
                TarifValut = 1800,
                TarifSom = 1800,
                Comiso = 0,
                ComisoSom = 0,
                ItogoPoBiletu = 1800,
                ItogoAv = 1800,
                Skidka = 0,
                PayPass = 1800
            });


            list.Add(new Ticket
            {
                Id = 3,
                Date = DateTime.Now,
                Company = new Company(),
                Number = "1382401416510",
                Passanger = "AOBULI",
                Reis = "аю477",
                Kurs = 1,
                TarifValut = 9117,
                TarifSom = 9117,
                Toplivo = 3419,
                Aeroport = 1024,
                Comiso = 638.19M,
                ComisoSom = 638.19M,
                ItogoPoBiletu = 13560,
                ItogoAv = 12921,
                Skidka = 0,
                PayPass = 13560
            });


            list.Add(new Ticket
            {
                Id = 4,
                Date = DateTime.Now,
                Company = new Company(),
                Number = "1382401416511",
                Passanger = "AISAN",
                Reis = "аю477",
                Kurs = 1,
                TarifValut = 9117,
                TarifSom = 9117,
                Toplivo = 3419,
                Aeroport = 1024,
                Comiso = 638.19M,
                ComisoSom = 638.19M,
                ItogoPoBiletu = 13560,
                ItogoAv = 12921,
                Skidka = 0,
                PayPass = 13560
            });

            return list;
        }

        private static Ticket GetNewTicket()
        {
            return new Ticket
            { 
                Id = 1,
                Date = DateTime.Now,
                Company = new Company(),
                Number = "1382401416177",
                Passanger = "ГАЕВ",
                Reis = "аю915",
                Kurs = 1,
                TarifValut = 8357,
                TarifSom = 8357,
                Comiso = 1024,
                ComisoSom = 1024,
                ItogoPoBiletu = 10901,
                ItogoAv = 10316,
                Skidka = 0,
                Toplivo = 1520,
                Aeroport = 1024,
                PayPass = 10901
            };
        }

        public static List<RefundTicket> GetRefundTickets()
        {
            var list = new List<RefundTicket>();

            list.Add(new RefundTicket
            {
                Ticket = GetNewTicket() ,
                Id = 1,
                Amount = GetNewTicket().PayPass,
                Date = DateTime.Now
            });

            return list;
        }

        public static Client GetClient()
        {
            return new Client
            {
                Id = 1,
                FirstName = "first name",
                Name = "name"
            };
        }

        public static Employee GetEmployee()
        {
            return new Employee
            {
                FirstName = "first name",
                Id = 1,
                Login = "login",
                Name = "name",
                Password = "password",
                Role = Role.Administrator
            };
        }

        public static RefundTicket GetRefundTicket()
        {
            return new RefundTicket
            {
                Ticket =  GetNewTicket(),
                Id = 1,
                Amount = 1000,
                Date = DateTime.Now
            };
        }
    }
}
