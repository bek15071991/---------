﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avia.BLL.Interfaces;
using Avia.BLL.Service;
using Avia.Domain.Entyties;
using Avia.Domain.People;
using Avia.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestProject1.Tests
{
    [TestClass]
    public class DaylyReportTest
    {
        [TestMethod]
        public void GetSalledTickets()
        {
            var mockDb = new Mock<IUnitofWork>();
            var mockUser = new Mock<IServiceUser>();
            var reporter = new DaylyReport(mockDb.Object, mockUser.Object);
            var ticket1 = new Ticket { PayPass = 10700, Employee = new Employee() };
            var ticket2 = new Ticket { PayPass = 2900, Employee = new Employee() };

            var creditTicekt = new Ticket
            {
                PayPass = 23000,
                Employee = new Employee(),
                Credit = new Credit { Amount = 10000 }
            };

            mockUser.Setup(d => d.GetCurrentUser()).Returns(new Employee());

            mockDb.Setup(d => d.Tickets.GetNotReportSalled(It.IsAny<int>())).Returns(new List<Ticket>
            {
                ticket1, ticket2 
            });

            mockDb.Setup(d => d.Tickets.GetNotReportCredited(It.IsAny<int>())).Returns(new List<Ticket> {creditTicekt});
             
            mockDb.Setup(d => d.RefundTickets.GetNotReport(It.IsAny<int>())).Returns(new List<RefundTicket>
            {
                new RefundTicket
                {
                    Ticket = new Ticket { PayPass = 8900, Employee = new Employee()},
                    Amount = 8900
                },
                new RefundTicket
                {
                    Ticket = new Ticket { PayPass = 15300, Employee = new Employee()},
                    Amount = 5000
                }
            });

            reporter.MakeReport();
            var list = reporter.GetSoldTickets();

            Assert.AreEqual(2, list.Count);

            CollectionAssert.Contains(list, ticket1);
            CollectionAssert.Contains(list, ticket2);
            CollectionAssert.DoesNotContain(list, creditTicekt);

            Assert.AreEqual(reporter.GetCash(),9700);
        }
    }
}
