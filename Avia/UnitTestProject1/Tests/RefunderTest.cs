﻿using System;
using System.Linq;
using Avia.BLL;
using Avia.BLL.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestProject1.Uitils;

namespace UnitTestProject1.Tests
{
    [TestClass]
    public class RefunderTest : BaseTest
    {
        private RefunderTicket _refunder;

        [TestInitialize]
        public void Setup()
        {
            _refunder = new RefunderTicket(_unit, _service.Object);
        }

        [TestMethod]
        public void TestRefund_fail_parameters()
        {
            ExceptionMessage es = _refunder.RefundTicket(1);

            Assert.IsTrue(es.IsExcept);

            Assert.AreEqual(es.Message, "Не найден билет");
        }

        [TestMethod]
        public void TestRefund_valid()
        {
            var ticket = FactoryObject.GetTicket();
            _unit.Tickets.Create(ticket);

            var es = _refunder.RefundTicket(ticket.Id);

            Assert.AreEqual(_unit.RefundTickets.GetAll().ToList().Count, 1);
            Assert.AreEqual(es.Message, "Возврат успешно проведен");
            var refundTicket = _unit.RefundTickets.GetNoTrack(1);
            Assert.AreEqual(refundTicket.Ticket, ticket);
            Assert.AreEqual(refundTicket.Date.Day, DateTime.Now.Day);

        }
         
        [TestMethod]
        public void TestRefund_cash_fail_parameters()
        {
            ExceptionMessage es = _refunder.RefundTicket(1, 30);

            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не найден билет");

            _unit.Tickets.Create(FactoryObject.GetTicket());
            es = _refunder.RefundTicket(1, 0);

            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не указан процент");

            es = _refunder.RefundTicket(1, 101);

            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Процент указан не правильно");

        }

        [TestMethod]
        public void TestRefund_cash_valid()
        { 
            _unit.Tickets.Create(FactoryObject.GetTicket());
            var es = _refunder.RefundTicket(1, 20);

            var refund = _unit.RefundTickets.GetNoTrack(1);
             
            Assert.IsFalse(es.IsExcept);
            Assert.IsNotNull(refund);
            Assert.AreEqual(refund.Amount, 1984m); 
            Assert.AreEqual(es.Message, "Возврат успешно проведен");

        }
    }
}
