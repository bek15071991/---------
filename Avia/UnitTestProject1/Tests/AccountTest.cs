﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avia.BLL;
using Avia.BLL.Service;
using Avia.Domain.Enum;
using Avia.Domain.People;
using Avia.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UnitTestProject1.Uitils;

namespace UnitTestProject1.Tests
{
    [TestClass]
    public class AccountTest
    {

        IUnitofWork _unitOfWork;
        AccountManager _manager;
        private Mock<IUnitofWork> _mock;

        [TestInitialize]
        public void AccountSetup()
        {
            _mock = new Mock<IUnitofWork>();
            var testEmplo = new Employee
            {
                Id = 1,
                FirstName = "test",
                Login = "login",
                Name = "def",
                Password = "password",
                Role = Role.Cassier
            };

            _mock.Setup(d => d.Employes.GetByLoginPassword("login", "password"))
                .Returns(testEmplo);

            _mock.Setup(d => d.Employes.GetByLogin("login")).Returns(testEmplo);
            _mock.Setup(d => d.Employes.GetNoTrack(1)).Returns(testEmplo);

            _mock.Setup(d => d.Employes.GetAll()).Returns(new List<Employee> { testEmplo }.AsQueryable());

            _unitOfWork = _mock.Object;
            _manager = new AccountManager(_unitOfWork);
        }


        [TestMethod]
        public void AuthorizeTest()
        {

            var eml = _manager.Athorize("asd", "password");

            Assert.AreEqual(eml, null);

            var empl = _manager.Athorize("login", "password");

            Assert.AreEqual(empl.Login, "login");


        }

        [TestMethod]
        public void GetByLoginTest()
        {
            var emp = _manager.GetByLogin("");
            Assert.IsNull(emp);
            emp = _manager.GetByLogin("login");
            Assert.AreEqual("login", emp.Login);
        }

        [TestMethod]

        public void CreateAccount_invalid_login_Test()
        {
            ExceptionMessage es = _manager.CreateAccount("", "password", "name", "first name", Role.Administrator);
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не указан логин");
        }

        [TestMethod]
        public void CreateAccount_invalid_password_Test()
        {
            ExceptionMessage es = _manager.CreateAccount("login", "", "name", "first name", Role.Administrator);
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не указан пароль");
        }

        [TestMethod]
        public void CreateAccount_invalid_name_Test()
        {
            ExceptionMessage es = _manager.CreateAccount("login", "password", "", "first name", Role.Administrator);
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не указана имя");
        }

        [TestMethod] 
        public void CreateAccount_invalid_firstName_Test()
        {
            ExceptionMessage es = _manager.CreateAccount("login", "password", "name", "", Role.Administrator);
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не указана фамилия");
        }


        [TestMethod]
        public void CreateAccount_invalid_role_less_Test()
        {
            ExceptionMessage es = _manager.CreateAccount("login1", "password", "name", "first name", (Role)(-1));
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не указан роль");
        }

        [TestMethod]
        public void CreateAccount_invalid_role_more_Test()
        {
            ExceptionMessage es = _manager.CreateAccount("login1", "password", "name", "first name", (Role)(Enum.GetNames(typeof(Role)).Length));
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не указан роль");
        }

        [TestMethod]
        public void CreateAccount_exist_account_Test()
        {
            ExceptionMessage es = _manager.CreateAccount("login", "password", "name", "first name", Role.Administrator);
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Такой аккаунт существует");
        }


        [TestMethod]
        public void CreateAccountTest()
        {
            ExceptionMessage es = _manager.CreateAccount("login2", "password", "name", "first name", Role.Administrator);
            Assert.IsFalse(es.IsExcept);
            Assert.AreEqual(es.Message, "Аккаунт был успешно сохранен");
            _mock.Verify(d => d.Employes.Create(It.IsAny<Employee>()));
            _mock.Verify(d => d.Save());
        }

        [TestMethod]
        public void ChangePasswordTest_fail_parametrs()
        {
            ExceptionMessage es = _manager.ChangePasswordByLogin("", "");
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Логин или пароль не были указаны");

            es = _manager.ChangePasswordByLogin("login", "");
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Логин или пароль не были указаны");

            es = _manager.ChangePasswordByLogin("", "newpassword");
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Логин или пароль не были указаны");

        }

        [TestMethod]
        public void ChangePasswoedTest_fake_login()
        {
            var es = _manager.ChangePasswordByLogin("lo", "newpassword");
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Аккаунт не был найден");
        }

        [TestMethod]
        public void ChangePasswordTest_valid()
        {
            _mock.Setup(d => d.Employes.GetByLogin("test")).Returns(new Employee());
            var es = _manager.ChangePasswordByLogin("login", "newpass");
            _mock.Verify(d => d.Employes.Update(It.IsAny<Employee>()));
            _mock.Verify(d => d.Save());
            Assert.IsFalse(es.IsExcept);
            Assert.AreEqual(es.Message, "Пароль успешно изменен");
        }


        [TestMethod]
        public void ChangeLoginById_fail_parametrs()
        {
            var es = _manager.ChangeLoginById(0, "sdf");

            Assert.AreEqual(es.Message, "Аккаунт не найден");
            Assert.IsTrue(es.IsExcept);

            _mock.Setup(d => d.Employes.GetNoTrack(1)).Returns(FactoryObject.GetEmployee());
            es = _manager.ChangeLoginById(1, "");
            Assert.IsTrue(es.IsExcept);
            Assert.AreEqual(es.Message, "Не указан новый логин");


        }

        [TestMethod]
        public void ChangeLoginById_valid()
        {
            _mock.Setup(d => d.Employes.GetNoTrack(1)).Returns(FactoryObject.GetEmployee());
          var es =  _manager.ChangeLoginById(1, "newlogin");

           Assert.IsFalse(es.IsExcept);
            Assert.AreEqual(es.Message,"Логин успешно изменен");

            _mock.Verify(d => d.Employes.Update(It.IsAny<Employee>()));
            _mock.Verify(d => d.Save());
        
        }
    }
}
