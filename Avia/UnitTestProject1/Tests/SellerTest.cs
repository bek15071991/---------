﻿using System.Collections.Generic;
using System.Linq;
using Avia.BLL.Interfaces;
using Avia.BLL.Service;
using Avia.Domain.Entyties;
using Avia.Domain.People;
using Avia.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UnitTestProject1.Uitils;

namespace UnitTestProject1.Tests
{
    [TestClass]
    public class SellerTest
    {
        private IUnitofWork _unit;
        private Seller _saller;
        private Mock<IUnitofWork> _mock;
        private Mock<IServiceUser> _serviceMock;
        private Ticket _testTicket;
        private Client _testClient;

        [TestInitialize]
        public void Setup()
        {
            _mock = FactoryObject.GetDb();

            _unit = FactoryObject.GetUnit();
            _serviceMock = new Mock<IServiceUser>();

            _saller = new Seller(_unit, _serviceMock.Object);
            _serviceMock.Setup(s => s.GetCurrentUser()).Returns(FactoryObject.GetEmployee());

            _testClient = FactoryObject.GetClient();

            _mock.Setup(d => d.Clients.Exists(1)).Returns(true);
            _mock.Setup(d => d.Credits.GetAll()).Returns(new List<Credit>().AsQueryable());
            _mock.Setup(d => d.Tickets.GetAll()).Returns(new List<Ticket> { new Ticket() }.AsQueryable());
            _testTicket = FactoryObject.GetTicket();


        }

        #region SaleTicket

        [TestMethod]
        public void SaleTicketTest()
        {
            var mes = _saller.SaleTicket(_testTicket);
            Assert.AreEqual(mes.IsExcept, false);
            _serviceMock.Verify(f => f.GetCurrentUser());
            CollectionAssert.Contains(_unit.Tickets.GetAll().ToList(), _testTicket);
        }

        [TestMethod]
        public void SaleTicketTest_fail_ticket()
        {
            var mes = _saller.SaleTicket(null);
            Assert.AreEqual(mes.IsExcept, true);
        }

        #endregion

        #region SaleTicketOnCredit

        [TestMethod]
        public void SaleTicketOnCreditTest()
        {
            _unit.Clients.Create(_testClient);
            var es = _saller.SaleTicketOnCredit(_testTicket, _testClient);

            Assert.AreEqual(es.IsExcept, false);
            CollectionAssert.Contains(_unit.Tickets.GetAll().ToList(), _testTicket);
            Assert.AreEqual(_unit.Credits.GetAll().ToList().Count, 1);
              
        }

        [TestMethod]
        public void SaleTicketOnCreditTest_null_ticket_client()
        {
            var es = _saller.SaleTicketOnCredit(null, _testClient);

            Assert.AreEqual(es.Message, "Не указан билет");

            es = _saller.SaleTicketOnCredit(_testTicket, null);

            Assert.AreEqual(es.Message, "Не указан клиент");

        }

        [TestMethod]
        public void SaleTicketOnCreditTest_fail_client()
        {
            _unit.Clients.Create(new Client()); 
            var es = _saller.SaleTicketOnCredit(_testTicket, _testClient);

            Assert.AreEqual(es.Message, "Не найден указанный клиент");

        }

        [TestMethod]
        public void SaleTicketOnCreditTest_fail_client_argument()
        {
            var es = _saller.SaleTicketOnCredit(_testTicket, null);

            Assert.AreEqual(es.IsExcept, true);
            Assert.AreEqual(es.Message, "Не указан клиент");

        }

        #endregion



        #region SaleOnCreditCash

        [TestMethod]
        public void SaleTicketOnCreditCashTest_null_ticket()
        {
            var es = _saller.SaleTicketOnCredit(null, _testClient, 500, "");

            Assert.AreEqual(es.IsExcept, true);
            Assert.AreEqual(es.Message, "Не указан билет");
        }

        [TestMethod]
        public void SaleTicketOnCreditCashTest_null_client()
        {
            var es = _saller.SaleTicketOnCredit(_testTicket, null, 500, "");

            Assert.AreEqual(es.IsExcept, true);
            Assert.AreEqual(es.Message, "Не указан клиент");
        }

        [TestMethod]
        public void SaleTicketOnCreditCashTest_fail_client()
        {
            var es = _saller.SaleTicketOnCredit(_testTicket, new Client(), 500, "");

            Assert.AreEqual(es.IsExcept, true);
            Assert.AreEqual(es.Message, "Не найден указанный клиент");
        }

        [TestMethod]
        public void SaleTicketOnCreditCashTest_fail_cash()
        {
            _unit.Clients.Create(_testClient);
            var es = _saller.SaleTicketOnCredit(_testTicket, _testClient, 0, "");

            Assert.AreEqual(es.IsExcept, true);
            Assert.AreEqual(es.Message, "Не указанна сумма платежа");

        }

        [TestMethod]
        public void SaleTicketOnCreditCashTest_cash_more_amount()
        {
            _unit.Clients.Create(_testClient);
            var es = _saller.SaleTicketOnCredit(_testTicket, _testClient, 15000, "");

            Assert.AreEqual(es.IsExcept, true);
            Assert.AreEqual(es.Message, "Оплата превышает сумму за билет");


        }

        #endregion

    }
}
