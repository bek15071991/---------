﻿using System.Linq;
using Avia.BLL;
using Avia.BLL.Interfaces;
using Avia.BLL.Service;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UnitTestProject1.Uitils;

namespace UnitTestProject1.Tests
{
    [TestClass]
    public class RepaymenterTest
    {
        private Repaymenter _repaymenter;

        private readonly Mock<IUnitofWork> _mock = FactoryObject.GetDb();
        private Mock<IServiceUser> _service;
        private IUnitofWork _unit = FactoryObject.GetUnit();

        [TestInitialize]
        public void Setup()
        {
            _service = new Mock<IServiceUser>();
            _repaymenter = new Repaymenter(_mock.Object, _service.Object);
        }

        [TestMethod]
        public void RepayTest_fail_income_parametrs()
        {
            _mock.Object.Tickets.Create(FactoryObject.GetTicket());

            ExceptionMessage es = _repaymenter.Repay(0, 1, "");

            Assert.AreEqual(es.Message, "Не указана сумма погашения");

            es = _repaymenter.Repay(320, 0, "");

            Assert.AreEqual(es.Message, "Не указан билет");

            es = _repaymenter.Repay(320, 2, "");

            Assert.AreEqual(es.Message, "Не указан билет");

        }


        [TestMethod]
        public void RepayTest_is_pay()
        {
            var ticket = FactoryObject.GetTicket();
            ticket.IsPay = true;
            _mock.Setup(d => d.Tickets.Get(1)).Returns(ticket);
            ExceptionMessage es = _repaymenter.Repay(350, 1, "");

            Assert.AreEqual(es.Message, "Билет уже оплачен");
        }

        [TestMethod]
        public void RepayTest_amount_more_credit()
        {
            var ticket = FactoryObject.GetTicket();
            var credot = new Credit
            {
                Amount = ticket.PayPass, 
                Id = 1
            };
      //      credot.Tickets.Add(ticket);
            credot.Repayments.Add(new Repayment() {  Id = 1, Amount = 5000 });
            credot.Repayments.Add(new Repayment() {   Id = 2, Amount = 2000 });
            _mock.Setup(d => d.Tickets.Get(1)).Returns(ticket);
            var es = _repaymenter.Repay(5000, ticket.Id, "");

            Assert.AreEqual(es.IsExcept, true);
            Assert.AreEqual(es.Message, "Сумма погашение превышает долг");
        }

        [TestMethod]
        public void RepayTest_valid()
        {
            var ticket = FactoryObject.GetTicket();
            var credot = new Credit
            {
                Amount = ticket.PayPass, 
                Id = 1
            };
         //    credot.Tickets.Add(ticket);
            credot.Repayments.Add(new Repayment() {   Id = 1, Amount = 500 });
            credot.Repayments.Add(new Repayment() {   Id = 2, Amount = 2000 });

            _repaymenter = new Repaymenter(_unit, _service.Object);
            _unit.Tickets.Create(ticket); 
            _repaymenter.Repay(5000, 1, "test");
               
            Assert.AreEqual(_unit.Repayments.GetAll().ToList().Count, 1);
            var credit = _unit.Repayments.GetAll().First();
            Assert.AreEqual(credit.Amount, 5000);
        }

    }
}
