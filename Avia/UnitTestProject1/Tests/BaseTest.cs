﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avia.BLL.Interfaces;
using Avia.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UnitTestProject1.Uitils;

namespace UnitTestProject1.Tests
{
    [TestClass]
    public abstract class BaseTest
    {
        protected IUnitofWork _unit = FactoryObject.GetUnit();
        protected Mock<IUnitofWork> _mock = FactoryObject.GetDb();
        protected Mock<IServiceUser> _service = new Mock<IServiceUser>();

    }
}
