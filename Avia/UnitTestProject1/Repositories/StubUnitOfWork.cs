﻿using System.Reflection;
using Avia.Domain.Repositories;

namespace UnitTestProject1.Repositories
{
    class StubUnitOfWork : IUnitofWork
    {
        private readonly IClientRepository _clientRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly IEmployeRepository _employeRepository;
        private readonly ICreditRepository _creditRepository;
        private readonly IRepaymentRepository _repaymentRepository;
        private readonly IRefundTicketRepository _refundTicketRepository;

        public StubUnitOfWork()
        {
            _clientRepository = new StubClientRepository();
            _creditRepository = new StubCreditRepository();
            _employeRepository = new StubEmployeRepository();
            _repaymentRepository = new StubRepaymentRepositories();
            _ticketRepository = new StubTicketRepository();
            _refundTicketRepository = new StubRefundTicketRepository();
        }

        public void Dispose()
        {

        }

        public IClientRepository Clients
        {
            get { return _clientRepository; }
        }
        public ITicketRepository Tickets { get { return _ticketRepository; } }
        public IEmployeRepository Employes { get { return _employeRepository; } }
        public ICreditRepository Credits { get { return _creditRepository; } }
        public IRepaymentRepository Repayments { get { return _repaymentRepository; } }
        public IRefundTicketRepository RefundTickets { get { return _refundTicketRepository; } }
        public ICompanyRepositories Companies { get; }

        public void Save()
        {
        }
    }
}
