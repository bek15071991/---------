﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace UnitTestProject1.Repositories
{
    public class StubRefundTicketRepository : StubBaseRepository<RefundTicket>, IRefundTicketRepository
    {
        public override void Update(RefundTicket t)
        { 
            List.ForEach(ticket =>
            {
                if (ticket.Id == t.Id)
                    ticket = t; 
            }); 
        }

        public override RefundTicket GetNoTrack(int id)
        {
            return List.Find(d => d.Id.Equals(id));
        }

        public List<RefundTicket> GetNotReport(int userId)
        {
            throw new NotImplementedException();
        }

        public List<RefundTicket> GetNotReportRefunded(int userId)
        {
            throw new NotImplementedException();
        }

        public override bool Exists(int id)
        {
            return List.Find(d => d.Id.Equals(id)) != null;
        }

        public IQueryable<RefundTicket> GetListIsNotReport()
        {
            return GetAll().Where(d => d.IsReport == false);
        }

        public RefundTicket GetIsNotReport(int id)
        {
            throw new NotImplementedException();
        }
    }
}
