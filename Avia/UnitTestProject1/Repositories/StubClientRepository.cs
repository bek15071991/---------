﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avia.Domain.People;
using Avia.Domain.Repositories;

namespace UnitTestProject1.Repositories
{
    class StubClientRepository : StubBaseRepository<Client>, IClientRepository
    {
        public override void Update(Client t)
        {
            var rep = List.Find(d => d.Id.Equals(t.Id));
            rep = t;
        }

        public override Client GetNoTrack(int id)
        {
            return List.Find(d => d.Id.Equals(id));
        }

        public override bool Exists(int id)
        {
            return List.Find(d => d.Id.Equals(id)) != null;
        }
    }
}
