﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace UnitTestProject1.Repositories
{
    public class StubCreditRepository : StubBaseRepository<Credit>, ICreditRepository
    {
        public override void Update(Credit t)
        {
            var rep = List.Find(d => d.Id.Equals(t.Id));
            rep = t;
        }

        public Credit GetByTicketId(int id)
        {
            throw new NotImplementedException();
        }

        public override Credit GetNoTrack(int id)
        {
            return List.Find(d => d.Id.Equals(id));
        }

        public override bool Exists(int id)
        {
            return List.Find(d => d.Id.Equals(id)) != null;
        }
    }
}
