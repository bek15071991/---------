﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avia.Domain.Repositories;
using Castle.Components.DictionaryAdapter;

namespace UnitTestProject1.Repositories
{
    public abstract class StubBaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected List<T> List = new EditableList<T>();
        public IQueryable<T> GetAll()
        {
            return List.AsQueryable();
        }

        public void Create(T t)
        {
            List.Add(t);
        }

        public abstract void Update(T t);

        public void Delete(T t)
        {
            List.Remove(t);
        }

        public void SaveChanges()
        {
           
        }

        public T Get(int id)
        {
            throw new NotImplementedException();
        }

        public abstract T GetNoTrack(int id);

        public abstract bool Exists(int id);
    }
}
