﻿using System.Collections.Generic;
using System.Linq;
using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace UnitTestProject1.Repositories
{
    class StubTicketRepository : StubBaseRepository<Ticket>, ITicketRepository
    {
        public override void Update(Ticket t)
        {
            var rep = List.Find(d => d.Id.Equals(t.Id));
            rep = t;
        }

        public override Ticket GetNoTrack(int id)
        {
            return List.Find(d => d.Id.Equals(id));
        }

        public override bool Exists(int id)
        {
            return List.Find(d => d.Id.Equals(id)) != null;
        }

        public IQueryable<Ticket> GetListIsNotReportIsNotCredit()
        {
            throw new System.NotImplementedException();
        }

        public Ticket GetNotRefundByNumber(string value)
        {
            throw new System.NotImplementedException();
        }

        public Ticket GetNotRefundById(int id)
        {
            throw new System.NotImplementedException();
        }

        public List<Ticket> GetListNotReportCredit()
        {
            throw new System.NotImplementedException();
        }

        public List<Ticket> GetNotReportSalled(int userId)
        {
            throw new System.NotImplementedException();
        }

        public List<Ticket> GetNotReportCredited(int userId)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Ticket> GetListIsNotReport(int id)
        {
            return GetAll().Where(d => d.IsReport == false);
        }

        public IQueryable<Ticket> GetListIsNotReport()
        {
            throw new System.NotImplementedException();
        }

        public Ticket GetIsNotReport(int idTicket)
        {
            throw new System.NotImplementedException();
        }
    }
}
