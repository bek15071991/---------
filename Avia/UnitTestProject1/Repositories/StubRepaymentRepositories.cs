﻿using Avia.Domain.Entyties;
using Avia.Domain.Repositories;

namespace UnitTestProject1.Repositories
{
    public class StubRepaymentRepositories : StubBaseRepository<Repayment>, IRepaymentRepository
    {
        public override void Update(Repayment t)
        {
           var rep = List.Find(d => d.Id.Equals(t.Id));
            rep = t;
        }

        public override Repayment GetNoTrack(int id)
        {
            return List.Find(d => d.Id.Equals(id));
        }

        public override bool Exists(int id)
        {
            return List.Find(d => d.Id.Equals(id)) != null;
        }
    }
}
