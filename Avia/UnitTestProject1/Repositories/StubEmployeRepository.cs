﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avia.Domain.People;
using Avia.Domain.Repositories;

namespace UnitTestProject1.Repositories
{
    class StubEmployeRepository : StubBaseRepository<Employee>, IEmployeRepository 
    {
        public override void Update(Employee t)
        {
            var rep = List.Find(d => d.Id.Equals(t.Id));
            rep = t;
        }

        public override Employee GetNoTrack(int id)
        {
            return List.Find(d => d.Id.Equals(id));
        }

        public override bool Exists(int id)
        {
            return List.Find(d => d.Id.Equals(id)) != null;
        }

        public Employee GetByLoginPassword(string login, string password)
        {
            return List.FirstOrDefault(d => d.Login.Equals(login) & d.Password.Equals(password));
        }

        public Employee GetByLogin(string login)
        {
            return List.FirstOrDefault(d => d.Login.Equals(login));
        }

       
    }
}
