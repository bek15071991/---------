﻿using System.ComponentModel;
using System.Reflection;

namespace Avia.Domain.Utils
{
    public  static class EnumUtil
    {
        public static string GetDescription(this System.Enum item)
        {
            var at = item.GetType().GetCustomAttribute(typeof (DescriptionAttribute));

            if (at != null)
                return ((DescriptionAttribute) at).Description;

            return string.Empty; 
        }
    }
}
