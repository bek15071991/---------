﻿using Avia.Domain.Entyties;
using Avia.Domain.Enum;
using System;
using System.Collections.Generic;

namespace Avia.Domain.People
{
    public class Employee : Person
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }

        public Employee()
        {
            Tickets = new List<Ticket>();
        }

        public Role Role { get; set; }

        public byte[] Image { get; set; }


        public string Image64 => Image == null ? string.Empty : $"data:{"image/png"};base64,{Convert.ToBase64String(Image)}";


        public string FullName => FirstName + " " + Name + " " + SecondName;
    }
}
