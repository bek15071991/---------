﻿using System.Collections.Generic;
using Avia.Domain.Entyties;

namespace Avia.Domain.People
{
    public class Client : Person
    {
        public virtual ICollection<Credit> Credits { get; set; }

        public Client()
        {
            Credits = new List<Credit>();
        }


    }
}
