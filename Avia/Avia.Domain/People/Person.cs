﻿using Avia.Domain.Entyties;

namespace Avia.Domain.People
{
    public class Person : Entity
    {
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string FullName => FirstName + " " + Name + " " + SecondName;
    }
}
