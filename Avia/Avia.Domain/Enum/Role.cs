﻿using System.ComponentModel;

namespace Avia.Domain.Enum
{
    public enum Role
    {
        [Description("Администратор")]
        Administrator,
        [Description("Авиакассир")]
        Cassier 
    }
}
