﻿using System;
using System.Linq;
using Avia.Domain.People;

namespace Avia.Domain.Entyties
{
    public class Ticket
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Reis { get; set; }
        public decimal TarifValut { get; set; }
        public decimal Kurs { get; set; }
        public decimal TarifSom { get; set; }
        public decimal Aeroport { get; set; }
        public decimal ItogoPoBiletu { get; set; }
        public decimal Comiso { get; set; }
        public decimal ComisoSom { get; set; }
        public decimal Skidka { get; set; }
        public decimal PayPass { get; set; }
        public decimal ItogoAv { get; set; }
        public decimal Toplivo { get; set; }
        public DateTime Date { get; set; }
        public bool IsReport { get; set; }
        public bool IsPay { get; set; }
        public virtual Employee Employee { get; set; }
        public string Passanger { get; set; }

        public virtual Credit  Credit  { get; set; }
  
        public virtual Company Company { get; set; }


        public decimal CreditAmount
        {
            get
            {
                decimal amount = 0;
                if (Credit!=null)
                {
                    amount = Credit.Amount - Credit.Repayments.Sum(d => d.Amount);
                }
                return amount;
            }
        }


        public virtual RefundTicket RefundTicket { get; set; }

    }
}
