﻿using System.Collections.Generic;
using Avia.Domain.People;


namespace Avia.Domain.Entyties
{
    public class Credit
    {
        
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public string Comment { get; set; }
          
        public virtual IList<Repayment> Repayments { get; set; }

        public virtual  Client Client { get; set; }

        public virtual  Ticket Ticket { get; set; } 

        public Credit()
        {
            Repayments = new List<Repayment>();
             
        }
    }
}
