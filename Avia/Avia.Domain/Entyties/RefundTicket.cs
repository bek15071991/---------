﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Castle.Components.DictionaryAdapter;

namespace Avia.Domain.Entyties
{
   public class RefundTicket
    {
       public int Id { get; set; }
  
       public virtual Ticket Ticket { get; set; }
          
       public decimal Amount { get; set; }

       public DateTime Date { get; set; }

       public bool IsReport { get; set; }

      
    }
}
