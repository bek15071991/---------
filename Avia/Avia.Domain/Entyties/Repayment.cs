﻿using System;

namespace Avia.Domain.Entyties
{
    public class Repayment
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime DateRepayment { get; set; }


        public virtual Credit Credit { get; set; }

        public string Comment { get; set; }

    }
}
