﻿using System.Collections.Generic;

namespace Avia.Domain.Entyties
{
    public class Company : Entity
    {
        public double Percent { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }

        public Company()
        {
                Tickets = new List<Ticket>();
        }


    }
}
