﻿using Avia.Domain.Entyties;

namespace Avia.Domain.Repositories
{
    public interface ICreditRepository : IBaseRepository<Credit>
    {
        Credit GetByTicketId(int id);
    }
}
