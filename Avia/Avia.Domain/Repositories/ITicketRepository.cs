﻿using System.Collections.Generic;
using Avia.Domain.Entyties;
using System.Linq;

namespace Avia.Domain.Repositories
{
    public interface ITicketRepository : IBaseRepository<Ticket>
    {
        IQueryable<Ticket> GetListIsNotReport();
        Ticket GetIsNotReport(int idTicket);
       
        bool Exists(int id);
        IQueryable<Ticket> GetListIsNotReportIsNotCredit();

        void Create(Ticket ticket);
        Ticket GetNotRefundByNumber(string value);
        Ticket GetNotRefundById(int id);
        List<Ticket> GetListNotReportCredit();
        List<Ticket> GetNotReportSalled(int userId);
        List<Ticket> GetNotReportCredited(int userId);
        IQueryable<Ticket> GetListIsNotReport(int idUser);
    }
}
