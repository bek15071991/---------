﻿using System;

namespace Avia.Domain.Repositories
{
    public interface IUnitofWork : IDisposable
    {
        IClientRepository Clients { get; }
        ITicketRepository Tickets { get;    }

        IEmployeRepository Employes { get;   }

        ICreditRepository Credits { get; }

        IRepaymentRepository Repayments { get;   }

        IRefundTicketRepository RefundTickets { get; }

        ICompanyRepositories Companies { get; }

        void Save();
    }
}
