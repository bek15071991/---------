﻿using Avia.Domain.Entyties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avia.Domain.Repositories
{
   public interface IRepaymentRepository :IBaseRepository<Repayment>
    {
    }
}
