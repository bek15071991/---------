﻿using System.Linq;

namespace Avia.Domain.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        void Create(T t);

        void Update(T t);

        void Delete(T t);

        void SaveChanges();

        T Get(int id);
    }
}
