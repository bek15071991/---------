﻿using Avia.Domain.People;

namespace Avia.Domain.Repositories
{
    public interface IEmployeRepository : IBaseRepository<Employee>
    {
        Employee GetByLoginPassword(string login, string password);
        Employee GetByLogin(string login);
        Employee GetNoTrack(int id);
    }
}
