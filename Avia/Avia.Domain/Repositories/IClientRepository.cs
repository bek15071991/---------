﻿using Avia.Domain.People;

namespace Avia.Domain.Repositories
{
    public interface IClientRepository : IBaseRepository<Client>
    {
        bool Exists(int id);
        Client GetNoTrack(int id);
    }
}
