﻿ 
using Avia.Domain.Entyties;

namespace Avia.Domain.Repositories
{
    public interface ICompanyRepositories : IBaseRepository<Company>
    {
        Company GetNoTrack(int companyId);
    }
}
