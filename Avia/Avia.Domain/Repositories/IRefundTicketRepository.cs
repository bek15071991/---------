﻿using System.Collections.Generic;
using System.Linq;
using Avia.Domain.Entyties;

namespace Avia.Domain.Repositories
{
    public interface IRefundTicketRepository : IBaseRepository<RefundTicket>
    {
        IQueryable<RefundTicket> GetListIsNotReport();

        RefundTicket GetIsNotReport(int id);
        RefundTicket GetNoTrack(int i);
        List<RefundTicket> GetNotReport(int userId);
    }
}
